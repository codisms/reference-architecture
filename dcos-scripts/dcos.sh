#!/bin/sh

vagrant_start() {
	if [ "$1" == "" ]; then
		vagrant up m1 a1 a2 p1 p2 boot
	else
		vagrant up $1
	fi
}

dcos_update() {
	vagrant_stop
	git pull
	curl -O https://downloads.dcos.io/dcos/EarlyAccess/dcos_generate_config.sh
}

cd ~/Development/dcos-vagrant
case $1 in
	auth) dcos auth login ;;
	update) dcos_update ;;
	start|up) vagrant_start $2 ;;
	stop|halt) vagrant halt $2 ;;
	pause|suspend) vagrant pause $2 ;;
	resume) vagrant resume $2 ;;
	status) vagrant status $2 ;;
	destroy|reset) vagrant destroy -f $2 ;;
	ssh) vagrant ssh $2 ;;
	*)
		echo Unknown action: $1
		echo $0 '{start|stop|pause|resume|status|reset|ssh|auth}'
		exit 1
		;;
esac

