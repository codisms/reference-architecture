#!/bin/sh

usage() {
	echo $0 '{install|uninstall|list} <service>'
	exit 1
}

list_apps() {
	echo ==[ Installed apps ]======================================================================
	dcos marathon app list
	echo
	echo ==[ Running tasks ]=======================================================================
	dcos marathon task list
	echo
	echo ==[ Available services ]==================================================================
	for n in _services/*.json; do
		n="${n##*/}"
		n="${n%.*}"
		if [ "$n" != "marathon-lb-internal" ]; then
			echo "  $n"
		fi
	done
}

install_marathonlb() {
	echo Installing marathon-lb-external...
	dcos package install --options=_services/marathon-lb-external.json marathon-lb
	echo Installing marathon-lb-internal...
	dcos package install --options=_services/marathon-lb-internal.json marathon-lb
}

uninstall_marathonlb() {
	echo Uninstalling /marathon-lb/external...
	dcos package uninstall /marathon-lb/external
	echo Uninstalling /marathon-lb/internal...
	dcos package uninstall /marathon-lb/internal
}

install_app() {
	echo Installing service $1...
	dcos marathon app add _services/$1.json
}

uninstall_app() {
	echo Uninstalling service $1...
	dcos marathon app remove $1
}

if [ "$1" == "" ]; then
	usage
fi

action=$1
#echo action = $action

case $action in
	install|uninstall) ;;
	list)
		list_apps
		exit 0
		;;
	*)
		echo Unknown action: $action
		usage
		;;
esac

if [ "$2" == "" ]; then
	usage
fi

shift
for service in "$@"; do
	#echo service = $service

	if [ "$service" == "marathon-lb" ]; then
		eval ${action}_marathonlb
	else
		if [ ! -f _services/$service.json ]; then
			echo Unknown service: $service
			exit 1
		fi

		eval ${action}_app ${service}
	fi
done

