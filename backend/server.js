'use strict';

(function dumpEnvironment() {
	console.log('==[ Environment ]=========================================');
	let envKeys = Object.keys(process.env);
	envKeys.sort();
	envKeys.forEach((key) => {
		if (key.substr(0, 4).toLowerCase() !== 'npm_') {
			console.log('  ', key, '=', JSON.stringify(process.env[key]));
		}
	});
	console.log('==========================================================');
})();

var path = require('path'),
	moment = require('moment');

var Math = require('./Math'),
	math = new Math();

function factorial(call, callback) {
	console.log(moment().utc().format('YYYY-MM-DD HH:mm:ss.SSS'), ' : ', 'factorial(', call.request, ')');
	//console.log('call.prototype', call.call.prototype);
	math.factorial(call.request.x, callback);
}

function power(call, callback) {
	console.log(moment().utc().format('YYYY-MM-DD HH:mm:ss.SSS'), ' : ', 'power(', call.request, ')');
	//console.log('call.prototype', call.call.prototype);
	math.power(call.request.x, call.request.y, callback);
}

(function setUpGrpcServer() {
	var grpc = require('grpc');

	function loadProto(server, serviceName, rpcMap) {
		let protoFile = path.join(__dirname, 'protos', serviceName + '.proto');
		console.log('Loading proto file %s...', protoFile);

		let descriptor = grpc.load(protoFile);
		//console.log('descriptor', descriptor);

		console.log('Configuring service %s:', serviceName, Object.keys(rpcMap));
		server.addProtoService(descriptor[serviceName].service, rpcMap);
	}

	var server = new grpc.Server();

	loadProto(server, 'Math', {
		factorial: factorial,
		power: power
	});

	var port = 50051;
	server.bind('0.0.0.0:' + port, grpc.ServerCredentials.createInsecure());
	//console.log('server', server);
	server.start();

	console.log('Listening on port ' + port);
})();

(function setUpKoaServer() {
	var koa = require('koa'),
		bodyParser = require('koa-bodyparser'),
		koaError = require('koa-error'),
		koaResponseTime = require('koa-response-time'),
		koaRouter = require('koa-router');

	var app = koa();

	app.use(function *log(next) {
		let start = moment();
		yield next;
		let end = moment(),
			diff = end.valueOf() - start.valueOf();
		console.log(start.utc().format('YYYY-MM-DD HH:mm:ss.SSS'), ' : ',
			this.request.method, this.request.url,
			' => ', this.response.status, diff + 'ms'
		);
	});
	app.use(koaResponseTime());
	app.use(bodyParser());
	app.use(koaError());

	var router = koaRouter();

	// define routes below

	router.post('/api/factorial', function *factorial() {
		math.factorial(this.request.body.x, (err, response) => {
			if (err) {
				this.status = 500;
				this.body = err;
				return;
			}

			this.status = 200;
			this.body = response;
		});
	});

	router.post('/api/power', function *power() {
		math.power(this.request.body.x, this.request.body.y, (err, response) => {
			if (err) {
				this.status = 500;
				this.body = err;
				return;
			}

			this.status = 200;
			this.body = response;
		});
	});

	// define routes above

	app.use(router.routes())
		.use(router.allowedMethods());

	var server = app.listen(9004, (err) => {
		if (err) {
			return console.error('Error on listen: ' + err);
		}
		let address = server.address();
		console.log('opened server on %s:%d', address.address, address.port);
	});
})();

