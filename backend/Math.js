'use strict';

module.exports = class MathImpl {
	constructor(/*options*/) {
		// do nothing
	}

	factorial(x, callback) {
		function _factorial(x) {
			if (x < 0) {
				throw new Error('Invalid argument');
			}
			if (x === 0) {
				return 1;
			}
			return x * _factorial(x - 1);
		}

		try {
			let result = _factorial(x);
			callback(null, { factorial: result });
		} catch (e) {
			callback(e);
		}
	}

	power(x, y, callback) {
		callback(null, { power: Math.pow(x, y) });
	}
};

