'use strict';

var path = require('path');

var grpc = require('grpc');

var protoFile = path.join(__dirname, '..', 'protos', 'Math.proto');
var descriptor = grpc.load(protoFile);
//console.log('descriptor', descriptor);
//var math = descriptor.Math;
//console.log('math', math);
var client = new descriptor.Math('localhost:50051', grpc.credentials.createInsecure());
//console.log('client', client);

console.log('Calling factorial()...');
client.factorial({ x: 5 }, function factorialCallback(err, result) {
	if (err) {
		return console.error('Received error:', err);
	}
	console.log('result = %j', result);
});
