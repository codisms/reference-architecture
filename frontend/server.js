'use strict';

(function dumpEnvironment() {
	console.log('==[ Environment ]=========================================');
	let envKeys = Object.keys(process.env);
	envKeys.sort();
	envKeys.forEach((key) => {
		if (key.substr(0, 4).toLowerCase() !== 'npm_') {
			console.log('  ', key, '=', JSON.stringify(process.env[key]));
		}
	});
	console.log('==========================================================');
})();

var path = require('path'),
	moment = require('moment');

var koa = require('koa'),
	bodyParser = require('koa-bodyparser'),
	koaError = require('koa-error'),
	koaResponseTime = require('koa-response-time'),
	koaRouter = require('koa-router'),
	koaStatic = require('koa-static');

var config = {
	host: process.env.MATH_HOST || 'localhost',
	grpcPort: process.env.MATH_GRPC_PORT || 50051,
	jsonPort: process.env.MATH_JSON_PORT || 9004
};

var MathGrpcClient = require('./Math.grpc'),
	MathJsonClient = require('./Math.json'),
	MathDirect = require('./Math'),
	mathClients = {
		grpc: new MathGrpcClient({
			host: config.host,
			port: config.grpcPort
		}),
		json: new MathJsonClient({
			host: config.host,
			port: config.jsonPort
		}),
		local: new MathDirect(),
		connectionTypes: [ 'local', 'grpc', 'json' ]
	};

var app = koa();

app.use(function *log(next) {
	let start = moment();
	yield next;
	let end = moment(),
		diff = end.valueOf() - start.valueOf();
	console.log(start.utc().format('YYYY-MM-DD HH:mm:ss.SSS'), ' : ',
		this.request.method, this.request.url,
		' => ', this.response.status, diff + 'ms'
	);
});
app.use(koaResponseTime());
app.use(bodyParser());
app.use(koaError());

function getClient(t, clients) {
	let clientType = clients.connectionTypes[0];
	if (t && t.request && t.request.query) {
		clients.connectionTypes.forEach((key) => {
			if (t.request.query.hasOwnProperty(key)) {
				clientType = key;
			}
		});
	}
	return mathClients[clientType];
}

var router = koaRouter();

// define routes below

router.post('/api/factorial', function *factorial() {
	// We have to use a promise because the grpc library uses and external binary
	// and Javascript's yield doesn't wait on it.
	yield new Promise((resolve, reject) => {
		let client = getClient(this, mathClients),
			x = this.request.body.x;
		console.log('Calling factorial(%j) [%s]...', x, client.constructor.name);
		client.factorial(x, (err, response) => {
			if (err) {
				//this.status = 500;
				//this.body = err;
				console.error(err);
				return reject(err);
			}

			this.status = 200;
			this.body = response;
			resolve();
		});
	});
});

router.post('/api/power', function *power() {
	// We have to use a promise because the grpc library uses and external binary
	// and Javascript's yield doesn't wait on it.
	yield new Promise((resolve, reject) => {
		let client = getClient(this, mathClients),
			x = this.request.body.x,
			y = this.request.body.y;
		console.log('Calling power(%j, %j) [%s]...', x, y, client.constructor.name);
		client.power(x, y, (err, response) => {
			if (err) {
				//this.status = 500;
				//this.body = err;
				console.error(err);
				return reject(err);
			}

			this.status = 200;
			this.body = response;
			resolve();
		});
	});
});

// define routes above

app.use(router.routes())
	.use(router.allowedMethods());

app.use(koaStatic(path.join(__dirname, 'static'), {
	gzip: true,
	yield: true
}));

var server = app.listen(9005, (err) => {
	if (err) {
		return console.error('Error on listen: ' + err);
	}
	let address = server.address();
	console.log('opened server on %s:%d', address.address, address.port);
});

