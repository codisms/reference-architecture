'use strict';

var request = require('request');

module.exports = class MathJsonClient {
	constructor(options) {
		console.log(`Using host ${options.host}:${options.port} for Math.json`);
		this._host = `http://${options.host}:${options.port}`;
	}

	factorial(x, callback) {
		request({
			method: 'POST',
			url: this._host + '/api/factorial',
			json: { x: x }
		}, (err, response, body) => {
			if (err) {
				return callback(err);
			}
			callback(null, body);
		});
	}

	power(x, y, callback) {
		request({
			method: 'POST',
			url: this._host + '/api/power',
			json: { x: x, y: y }
		}, (err, response, body) => {
			if (err) {
				return callback(err);
			}
			callback(null, body);
		});
	}
};

