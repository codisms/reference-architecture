
(function script($) {
	$(function onLoad() {
		function setUpOperation(name, parameters, submitCallback) {
			var div = $('div.operation#' + name),
				operation = {
					name: name,
					callback: submitCallback,
					inputs: [],
					output: div.find('.output'),
					outputValue: div.find('.output .value'),
					submit: div.find('.input button.submit')
				},
				i,
				max = parameters.length,
				parameter;

			for (i = 0; i < max; i++) {
				parameter = parameters[i];
				operation.inputs.push(div.find('.input input#' + name + '_' + parameter));
			}

			operation.submit.on('click', function onClick() {
				var i,
					max = operation.inputs.length,
					inputs = [];

				for (i = 0; i < max; i++) {
					inputs.push(operation.inputs[i].val());
				}

				submitCallback.apply(operation, inputs);
			});

			operation.setOutputValue = function setOutputValue(o) {
				console.log(operation.outputValue);
				operation.output.show();
				operation.outputValue.html(JSON.stringify(o));
			};

			return operation;
		}

		var apiType = $('#api-type');

		function getApiType() {
			var selected = apiType.find('input[type=radio]:checked');
			return selected.val();
		}

		setUpOperation('factorial', [ 'x' ], function factorial(x) {
			var self = this;

			console.log('factorial(' + x + ')');
			$.ajax({
				method: 'POST',
				url: '/api/factorial?' + getApiType(),
				data: JSON.stringify({ x: parseInt(x) }),
				dataType: 'json',
				contentType: 'application/json'
			}).done(function done(response) {
				self.setOutputValue(response);
			}).fail(function fail(response) {
				console.error('fail', response);
			});
		});

		setUpOperation('power', [ 'x', 'y' ], function power(x, y) {
			var self = this;

			console.log('power(' + x + ',' + y + ')');
			$.ajax({
				method: 'POST',
				url: '/api/power?' + getApiType(),
				data: JSON.stringify({ x: parseInt(x), y: parseInt(y) }),
				dataType: 'json',
				contentType: 'application/json'
			}).done(function done(response) {
				self.setOutputValue(response);
			}).fail(function fail(response) {
				console.error('fail', response);
			});
		});
	});
})(window.jQuery);

