'use strict';

var path = require('path');

var grpc = require('grpc');
var protoFile = path.join(__dirname, 'protos', 'Math.proto');
var descriptor = grpc.load(protoFile);

module.exports = class MathGrpcClient {
	constructor(options) {
		console.log(`Using host ${options.host}:${options.port} for Math.grpc`);
		this._client = new descriptor.Math(`${options.host}:${options.port}`, grpc.credentials.createInsecure());
	}

	factorial(x, callback) {
		this._client.factorial({ x: x }, callback);
	}

	power(x, y, callback) {
		this._client.power({ x: x, y: y }, callback);
	}
};

