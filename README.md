# Codisms Reference Architecture

## Services
This repository contains two sample projects that we'll use for demonstration.

Note that these services use [gRPC](http://www.grpc.io/) for remote invocation of methods.  gRPC is Google's RPC layer that uses protobufs for transmitting messages.  It's very fast and the messages themselves are small due to their being binary in nature.

There are two services available as defined in the Math protobuf definition [here](https://bitbucket.org/codisms/reference-architecture/src/master/protos/Math.proto):
```protobuf
service Math {
	rpc Factorial(FactorialRequest) returns (FactorialResponse) {};
	rpc Power(PowerRequest) returns (PowerResponse) {};
}

message FactorialRequest {
	required int32 x = 1;
}

message FactorialResponse {
	required int32 factorial = 1;
}

message PowerRequest {
	required int32 x = 1;
	required int32 y = 2;
}

message PowerResponse {
	required int32 pow = 1;
}
```
These methods are obviously trivial and are only used to demonstrate the system.

The [backend](https://bitbucket.org/codisms/reference-architecture/src/master/backend) service is used to demonstrate hosting a gRPC service.

The [frontend](https://bitbucket.org/codisms/reference-architecture/src/master/frontend) service is a standard web server serving a single page that has forms to represent the methods described in the service file above.  

There is also a switch in the web page to be able to specify whether to run locally at the frontend service, or to use RPC.  This is to demonstrate how to construct local procedures to make them ready for remote invocation.

These services are containerized using the Docker files provided and uploaded to [Docker Hub](https://hub.docker.com/r/codisms).

## Installation Walkthrough

### DC/OS + Vagrant Installation
Basic instructions are available on the [DC/OS installation page](https://dcos.io/docs/1.7/administration/installing/local/).  This will take you to the [dcos-vagrant](https://github.com/dcos/dcos-vagrant) repo.  Follow the steps outlined in the [Deploy](https://github.com/dcos/dcos-vagrant#deploy) section with these additions:

* For step 5, you can create a symlink to the appropriate config file.
* For step 6, create a symlink to the `VagrantConfig.yaml` file in this repository; it has minimal machine configurations to run multiple instances of all of the services being used.

Once everything is set up, you should be able to load the DC/OS page by visiting http://m1.dcos/.

### DC/OS CLI
Visit the [DC/OS CLI Installation page](https://docs.mesosphere.com/usage/cli/install/) for details on installing.

### Marathon LB
[Marathon-lb](https://github.com/mesosphere/marathon-lb) is used for service discovery and load balancing.  To install, we need both an internal and external instance.  You can do this by executing the following (relative to this file):

```
dcos package install --options=dcos-scripts/_services/marathon-lb-external.json marathon-lb

dcos package install --options=dcos-scripts/_services/marathon-lb-internal.json marathon-lb
```

Wait for the services to show as running in the [Marthon UI](http://m1.dcos/service/marathon/ui/#/apps) before continuing.  This can take awhile to start due to the size of the service.

(**Note:** The scripts in the `dcos-scripts` folder are for convenience and demonstration only.  It is recommended that you become accustomed with the existing command line tools.  As such, I will not cover these scripts.)

### frontend, backend
The frontend service is considered "external" while the backend service is considered "internal".  What this ends up meaning is that the appropriate load balancer is chosen.

To install:
```
dcos marathon app add dcos-scripts/_services/backend.json
dcos marathon app add dcos-scripts/_services/frontend.json
```

Once these are both running, you should be able to visit http://p1.dcos:10001/ to see the frontend UI.

### Voila!
You should now have an architecture that is distributed to multiple slaves.

## Quick Links

* [DC/OS UI](http://m1.dcos/)
* [Marthon UI](http://m1.dcos/service/marathon/ui/#/apps)
* [Internal haproxy stats](http://a1.dcos:9090/haproxy?stats)
* [External haproxy stats](http://p1.dcos:9090/haproxy?stats)
* [frontend](http://p1.dcos:10001/)

## References

* [Mesosphere Marathon Documentation](https://docs.mesosphere.com/usage/services/marathon/)
* [Marathon Tutorials](https://dcos.io/docs/1.7/usage/tutorials/marathon/)
* [Service discovery and load balancing with DCOS and marathon-lb](https://mesosphere.com/blog/2015/12/04/dcos-marathon-lb/)
* [Running Docker Containers on Marathon](https://mesosphere.github.io/marathon/docs/native-docker.html)
* [Marathon : Ports](http://mesosphere.github.io/marathon/docs/ports.html)
* [Marathon : Application Deployments](https://mesosphere.github.io/marathon/docs/deployments.html)
* [Continuous Delivery with Docker on Mesos in less than a minute](http://container-solutions.com/continuous-delivery-with-docker-on-mesos-in-less-than-a-minute/)
