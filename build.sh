#!/bin/sh

push=0
actions=0

build_node() {
	echo Build node images....
	docker build -f Dockerfile.node:v5.x -t codisms/node:v5.x .
	docker build -f Dockerfile.node:v6.x -t codisms/node:v6.x .

	if [ $push == 1 ]; then
		echo Pushing node images...
		docker push codisms/node:v5.x
		docker push codisms/node:v6.x
	fi
}

build_frontend() {
	echo Build frontend image....
	docker build -f Dockerfile.frontend -t codisms/frontend .

	if [ $push == 1 ]; then
		echo Pushing frontend image...
		docker push codisms/frontend
	fi
}

build_backend() {
	echo Build backend image....
	docker build -f Dockerfile.backend -t codisms/backend .

	if [ $push == 1 ]; then
		echo Pushing backend image...
		docker push codisms/backend
	fi
}

for var in "$@"; do
	if [ "$var" == "--push" ]; then
		push=1
		actions=$((actions - 1))
	fi
done

for var in "$@"; do
	case $var in
		"frontend")
			build_frontend
			;;
		"backend")
			build_backend
			;;
		"node")
			build_node
			;;
		"--push")
			;;
		*)
			echo Unknown action: $var
			exit 1
			;;
	esac
	actions=$((actions + 1))
done

echo Conducted $actions actions
push=
actions=

