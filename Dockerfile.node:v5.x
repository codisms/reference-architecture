FROM centos:latest
#FROM node:5.11.1-onbuild

RUN curl -sSL https://rpm.nodesource.com/setup_5.x | bash
RUN yum -y update
RUN yum install -y nodejs npm git git-core
RUN npm install -g npm

